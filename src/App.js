import React, {useState} from "react";
import tes1 from './Assets/images/Tesla/images/tes1.jpeg'
import tes2 from './Assets/images/Tesla/images/tes2.jpeg'
import tes3 from './Assets/images/Tesla/images/tes3.jpeg'
import tes4 from './Assets/images/Tesla/images/tes4.jpeg'
import tes5 from './Assets/images/Tesla/images/tes5.jpeg'
import white from './Assets/images/Tesla/images/white.jpg'

import css from './Style/css.css'

function App() {
  const [zamena, setZamena] = useState(0)

  const colors = [
    {car: tes1},
    {car: tes2},
    {car: tes3},
    {car: tes4},
    {car: tes5},
  ]

  const change = (i) => {
    setZamena(i)
  }


  return (
    <div>
      <img className={'car'} src={colors[zamena].car} alt="car"/>
      {
        colors.map((item,index) => {
          return (
              <>
                <div className={`img img${index + 1}`} onClick={() => change(index)} />
                </>
          )


        })
      }

    </div>
  );
}

export default App;
