import React, {useState} from "react";
import {Row,Col,Container,Card,Navbar,Button,Nav,NavDropdown,Form,} from "react-bootstrap";
import products from '../products'
const Photo = () => {

    const f = products.filter((foto) => {
        return foto.category_id === 206
    })

    return (
        <div>
            <Container>
                            <Row>
                                {
                                    f.map((foto) =>{
                                        return(
                                <Col kye={foto.id}  className={'col'}>
                                    <Card className={'mt-4'} style={{width:'15rem',margin:'20px',height:'23rem'}}>
                                        <div className={foto.discount ? 'skidka' : null}>{foto.discount}</div>
                                        <div className={'mt-2'} style={{backgroundImage: `url(${foto.main_image.path.original})` , width: '145px'  , backgroundPosition:'center', backgroundSize:'contain' , backgroundRepeat:'no-repeat' , margin:'auto', height:'200px'}}/><Card.Body>
                                        <Card.Title>{foto.title}</Card.Title>
                                        <Button style={{width: '100%'}} variant="primary">{foto.price} c </Button>
                                    </Card.Body>
                                    </Card>
                                </Col>
                                        )
                                    })
                                }
                            </Row>
                        </Container>

        </div>
    );
};

export default Photo;
