
import products from '../products'
import {Container, Row, Col, Card, Button} from "react-bootstrap";
import React, {useState} from "react";

function App(props) {
    const poisk = props.search;
    const [dollar , setDollar] = useState(84);
    return (
        <div className="App">
            <Container>
                <Row>
                    {
                        products.filter((item) =>{
                            return poisk.toLowerCase() === '' ? item : poisk.toLowerCase() !== '' ? item.title.toLowerCase().includes(poisk) : `${<p style={{zIndex: 2}}>не найден</p>}`
                        }).map((item) =>{
                            return(
                                <Col key={item.id}>
                                    <Card className={'mt-4'} >
                                        <div style={{backgroundImage: `url(${item.main_image.path.original})` , width: '290px'  , backgroundPosition:'center', backgroundSize:'contain' , backgroundRepeat:'no-repeat' , margin:'auto', height:'400px'}}/>
                                        <Card.Body>
                                            <Card.Title>{item.title}</Card.Title>
                                            <Button className={'btn'} onClick={() =>{
                                                setDollar(item.price/dollar)
                                            }} variant="primary">{item.price === item.price ? dollar : item.price} </Button>
                                        </Card.Body>
                                    </Card>
                                </Col>
                            )
                        })
                    }
                </Row>
            </Container>
        </div>
    );
}

export default App;