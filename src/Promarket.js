import './Style/css.css'
import React, {useState} from "react";
import tovary from "./products"
import {Row,Col,Container,Card,Navbar,Button,Nav,NavDropdown,Form,} from "react-bootstrap";
import categories from "./categories";
import {Routes, Route, Link} from 'react-router-dom'
import Smart from "./components/Smart";
import Naush from "./components/Naush";
import Laptop from "./components/Laptop";
import Watch from "./components/Watch";
import Photo from "./components/Photo";
import Home from './components/Home'


const Promarket = () => {
    const [disc , setDisc] = useState(true);
    const [sort , setSort] = useState(1);
    const [search , setSearch] = useState('')
    console.log(search);
    const filtred = tovary.filter((el)=>{
        return el.discount !== null
    })
    const change = () =>{
        setDisc(2)
    };
    let box = null

    categories.map((item) =>{
        box = item.id
    });

    const open = () =>{
        setDisc(3)
    };
    return (
        <div>
            <Navbar style={{backgroundColor: 'grey'}} expand="lg">
                <Container fluid>
                    <Navbar.Brand href="#" style={{color:'black'}}>ProMarket</Navbar.Brand>
                    <Navbar.Toggle aria-controls="navbarScroll" />
                    <Navbar.Collapse id="navbarScroll">
                        <Nav
                            className="me-auto my-2 my-lg-0"
                            style={{ maxHeight: '100px' }}
                            navbarScroll>
                            <Nav.Link onClick={open} href="" style={{color:'black'}} >Главная</Nav.Link>
                            <Nav.Link onClick={change} href="#action2" style={{color:'black'}}>Скидки</Nav.Link>
                            <NavDropdown style={{color:'white'}} title="Категории" id="navbarScrollingDropdown">
                                <Nav.Link href="" >
                                    <Link to={'/smart'}>Смартфоны и аксессуары</Link>
                                </Nav.Link>
                                <Nav.Link href="" >
                                    <Link to={'/laptop'}>Планшеты и электронные книги</Link>
                                </Nav.Link>
                                <Nav.Link href="" >
                                    <Link to={'/naush'}>Наушники и Bluetooth-гарнитуры</Link>
                                </Nav.Link>
                                <Nav.Link href="" >
                                    <Link to={'/watch'}>Смарт-часы и браслеты</Link>
                                </Nav.Link>
                                <Nav.Link href="" >
                                    <Link to={'/photo'}>Фото и видеокамеры</Link>
                                </Nav.Link>
                            </NavDropdown>
                        </Nav>
                        <Form className="d-flex">
                            <input
                                onChange={event => setSearch(event.target.value)}
                                type="search"
                                placeholder="Что ишете?"
                                className="me-2"
                                aria-label="Search"
                            />
                            <Button variant="outline-success">Найти</Button>
                        </Form>
                    </Navbar.Collapse>
                </Container>

            </Navbar>
            <Container>
                <Row className={'Row'}>
                    {
                        disc === 2 ? filtred.map((el) =>{
                            return(
                                <Col key={el.id} className={'col'}>
                                    <Card className={'mt-4'} style={{width:'18rem',margin:'20px',height:'23rem'}}>
                                        <div className={el.discount ? 'skidka' : null}>{el.discount}</div>
                                        <div className={'mt-2'} style={{backgroundImage: `url(${el.main_image.path.original})` , width: '145px'  , backgroundPosition:'center', backgroundSize:'contain' , backgroundRepeat:'no-repeat',margin:'auto',height:'200px'}}/><Card.Body>
                                        <Card.Title>{el.title}</Card.Title>
                                        <Button style={{width: '100%'}} variant="danger">{el.price}$</Button>
                                    </Card.Body>
                                    </Card>
                                </Col>
                            )
                        }) : disc === 3 ? tovary.map((item) =>{
                            return(
                                <Col key={item.id} className={'col'}>
                                    <Card className={'mt-4'} style={{width:'18rem',margin:'20px',height:'23rem'}}>
                                        <div className={item.discount ? 'skidka' : null}>{item.discount}</div>
                                        <div className={'mt-2'} style={{backgroundImage: `url(${item.main_image.path.original})` , width: '145px'  , backgroundPosition:'center', backgroundSize:'contain' , backgroundRepeat:'no-repeat' , margin:'auto', height:'200px'}}/><Card.Body>
                                        <Card.Title>{item.title}</Card.Title>
                                        <Button style={{width: '100%'}} variant="primary">{item.price} P</Button>
                                    </Card.Body>
                                    </Card>
                                </Col>
                            )
                        }) : tovary.filter((item) =>{
                            return search.toLowerCase() === '' ? item : item.title.toLowerCase().includes(search)
                        }).map((item) =>{
                            return(
                                <Col key={item.id} className={'col'}>
                                    <Card className={'mt-4'} style={{width:'15rem',margin:'20px',height:'23rem'}}>
                                        <div className={item.discount ? 'skidka' : null}>{item.discount}</div>
                                        <div className={'mt-2'} style={{backgroundImage: `url(${item.main_image.path.original})` , width: '145px'  , backgroundPosition:'center', backgroundSize:'contain' , backgroundRepeat:'no-repeat' , margin:'auto', height:'200px'}}/><Card.Body>
                                        <Card.Title>{item.title}</Card.Title>
                                        <Button style={{width: '100%'}} variant="primary">{item.price} c</Button>
                                    </Card.Body>
                                    </Card>
                                </Col>
                            )
                        })
                    }
                </Row>
            </Container>

            <Routes>
                <Route path={'/photo'} element={<Photo/>} />
                <Route path={'/naush'} element={<Naush/>}/>
                <Route path={'/laptop'} element={<Laptop/>}/>
                <Route path={'/smart'} element={<Smart/>}/>
                <Route path={'/watch'} element={<Watch/>}/>
            </Routes>


        </div>

    );
};

export default Promarket;
